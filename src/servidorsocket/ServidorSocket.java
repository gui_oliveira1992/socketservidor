package servidorsocket;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**Classe principal da aplicação servidor
 * @author Guilherme
 * @version 1.00
 * @since Release 00 da aplicação
 */
public class ServidorSocket {

    /**
     * Método Principal da aplicação
     *
     * @param args recebe entrada de dados
     * @author Guilherme
     */
    public static void main(String[] args) {

        List<ServerSocket> servList = new ArrayList<ServerSocket>();

        try {
            /**
             * Atribuindo/Definindo o número da porta de comunicação padrão do
             * servidor
             */
            int port = 1050;

            System.out.println("Incializando o servidor...");

            /**
             * Iniciliza o servidor
             */
            ServerSocket serv = new ServerSocket(port);
            servList.add(serv);
            System.out.println("Servidor iniciado, ouvindo a porta " + port);

            /**
             * Aguarda conexões
             */
            while (true) {
                Socket cliente = serv.accept();
                /**
                 * Inicializa thread do cliente
                 */
                if (cliente.isConnected()) {
                    ThreadCliente threadCliente = new ThreadCliente(cliente);
                    threadCliente.start();
                    threadCliente.executarServidor();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
