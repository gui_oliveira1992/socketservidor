
package servidorsocket;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**Classe de criação de thread e envio de mensagem via socket
 * @author Guilherme
 * @version 1.00
 * @since Release 00 da aplicação
 */
public class ThreadCliente extends Thread {

    
    private Socket cliente;

  /**
  * Construtor da classe
  * 
  * @param Socket      conexão do cliente socket
  * @author            Guilherme
  */
    public ThreadCliente(Socket cliente) {
        this.cliente = cliente;
    }

  /**
  * Envio da mensagem via Socket
  * 
  * @param void        Nulo
  * @author            Guilherme
  * @return            Nulo    
  */
    public void executarServidor() {
        
        try {
            /**
             * Instanciando o objeto responsável por enviar a mensagem via Socket
             */
            ObjectOutputStream saida = new ObjectOutputStream(cliente.getOutputStream());
            
            /**
             * Definido a mensagem de forma estática a ser enviada via socket
             */
            
            /**
             * Atribindo a mensagem ao objeto responsável por enviar a mensagem via socket
             */
            saida.writeUTF("MENSAGEM ENVIADA COM SUCESSO!");
            /**
             * Finalizando as instancias do envio da mensagem da comunicação server socket
             */
            saida.flush();
            saida.close();
            
        } catch (Exception e) {
            /**
             *  Está mensagem será exibida via console, caso ocorrer algum erro com a thread de comunicação
             */
            System.out.println("Excecao ocorrida na thread: " + e.getMessage());
            
        }
    }
}
